<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function lineitems()
    {
        return $this->hasMany('App\Lineitem');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
