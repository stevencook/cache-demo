<?php

namespace App\Providers;

use App\Charts\CrunchDay;
use App\Charts\SalesChart;
use App\Charts\Cache\DayCache;
use App\Charts\Cache\ChartCache;
use App\Charts\Interfaces\ChartInterface;
use App\Charts\Interfaces\DataCruncherInterface;

use Illuminate\Support\ServiceProvider;

class ChartsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ChartInterface::class, function($app) {
            // return new SalesChart;
            return new ChartCache(new SalesChart);
        });

        $this->app->singleton(DataCruncherInterface::class, function($app) {
            // return new CrunchDay;
            return new DayCache(new CrunchDay);
        });
    }
}
