<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MetricsController extends Controller
{
    public function index($start, $end) {
        $endpoint = route('product-sales', [$start, $end]);
        return view('results', [
            'endpoint' => $endpoint
        ]);
    }
}
