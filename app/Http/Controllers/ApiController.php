<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Charts\SalesChart;
use App\Charts\Helpers\GraphFormatter;

use App\Charts\Interfaces\ChartInterface;
use App\Charts\Interfaces\DataCruncherInterface;

class ApiController extends Controller
{
    public function productSales(ChartInterface $chart, DataCruncherInterface $day, GraphFormatter $graph, $start, $end) {

        // Crunch the numbers
        $data = $chart->getChartData($day, $start, $end);
        $labels = $chart->getChartLabels();

        // Convert data info ChartJS format
        return $graph->generateGraph($data, $labels);
    }

}
