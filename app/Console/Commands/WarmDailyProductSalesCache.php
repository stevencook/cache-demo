<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Charts\Interfaces\DataCruncherInterface;

use Carbon\Carbon;

class WarmDailyProductSalesCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:warm-daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill the cache with entries';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(DataCruncherInterface $day)
    {
        $this->info('Caching the past 30 days...');

        $lastDay = now();
        $currentDay = now()->subDays(30);
        $labels = [
            1 => 'Product A',
            2 => 'Product B',
            3 => 'Product C',
            4 => 'Product D',
        ];

        // Address each day
        while ($currentDay->lessThan($lastDay)) {

            // Cache data for this day
            $day->crunchData($currentDay, $labels);

            // Increment day
            $currentDay->addDay();
        }

        $this->info('Oh yeah, it’s cached');
    }
}
