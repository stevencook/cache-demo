<?php

namespace App\Charts\Interfaces;

interface ChartInterface {

    public function getChartData(DataCruncherInterface $day, $start, $end);
    public function getChartLabels();

}