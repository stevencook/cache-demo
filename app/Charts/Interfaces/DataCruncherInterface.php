<?php

namespace App\Charts\Interfaces;

interface DataCruncherInterface {

    public function crunchData($dayStart, $labels);

}