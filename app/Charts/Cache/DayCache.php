<?php

namespace App\Charts\Cache;

use App\Charts\Interfaces\DataCruncherInterface;

use Cache;

class DayCache implements DataCruncherInterface {

    private $next;

    public function __construct(DataCruncherInterface $next) {
        $this->next = $next;
    }

    public function crunchData($dayStart, $labels) {

        // Get and cache data for the day
        $cacheKey = md5($dayStart->format('Ymd') . http_build_query($labels));
        $minutes = 60;
        return Cache::remember($cacheKey, $minutes, function() use ($dayStart, $labels) {
            return $this->next->crunchData($dayStart, $labels);
        });
    }

}
