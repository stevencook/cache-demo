<?php

namespace App\Charts\Cache;

use App\Charts\Interfaces\ChartInterface;
use App\Charts\Interfaces\DataCruncherInterface;
use Cache;

class ChartCache implements ChartInterface {

    private $next;

    public function __construct(ChartInterface $next) {
        $this->next = $next;
    }

    public function getChartData(DataCruncherInterface $day, $start, $end) {

        // Get and cache data for the day
        $cacheKey = md5('full' . $start . $end);
        $minutes = 60;
        return Cache::remember($cacheKey, $minutes, function() use ($day, $start, $end) {
            return $this->next->getChartData($day, $start, $end);
        });

    }

    public function getChartLabels() {
        return $this->next->getChartLabels();
    }

}
