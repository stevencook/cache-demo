<?php

namespace App\Charts;

use App\Charts\Interfaces\DataCruncherInterface;

use Carbon\Carbon;
use DB;

class CrunchDay implements DataCruncherInterface {

    public function crunchData($dayStart, $labels) {
        $dayEnd = (new Carbon($dayStart))->addDay();

        // Create structure to hold results
        $dayData = [];
        foreach ($labels as $id => $title) {
            $dayData[$id] = [];
            $dayData[$id][$dayStart->format('M j')] = 0;
        }

        // Get lineitems in interval
        $lineitems = DB::table('lineitems')
                    ->join('orders', 'orders.id', '=', 'lineitems.order_id')
                    ->where('orders.created_at', '>=', $dayStart->format('Y-m-d'))
                    ->where('orders.created_at', '<', $dayEnd->format('Y-m-d'))
                    ->join('customers', 'customers.id', '=', 'orders.customer_id')
                    ->where('customers.country', '=', 'US')
                    ->select('product_id', 'price_per_unit_in_cents', 'quantity', 'orders.created_at')
                    ->get();

        // Add each lineitem to the lineitem total
        foreach ($lineitems as $lineitem) {
            $amount = $lineitem->quantity * $lineitem->price_per_unit_in_cents;
            $dayData[$lineitem->product_id][$dayStart->format('M j')] += $amount;
        }

        // Convert to dollars
        foreach ($dayData as &$dataProduct) {
            foreach ($dataProduct as &$dataEntry) {
                $dataEntry = number_format($dataEntry * 0.01, 2, '.', '');
            }
        }

        return $dayData;
    }

}