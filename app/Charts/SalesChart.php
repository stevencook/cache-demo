<?php

namespace App\Charts;

use App\Charts\Interfaces\ChartInterface;
use App\Charts\Interfaces\DataCruncherInterface;

use Carbon\Carbon;

class SalesChart implements ChartInterface {

    private $labels;

    public function __construct() {

        // Hard-code some products for the demo
        $this->labels = [
            1 => 'Product A',
            2 => 'Product B',
            3 => 'Product C',
            4 => 'Product D',
        ];
    }

    private function addDayToResults($dayData, $allDaysData) {

        // Merge this day's data into array of all days' data
        if ($allDaysData == null) {
            return $dayData;
        } else {
            // Merge dayData
            foreach ($allDaysData as $productId => &$dataProduct) {
                foreach ($dayData[$productId] as $dayDate => &$dayValue) {
                    $allDaysData[$productId][$dayDate] = $dayValue;
                }
            }
            return $allDaysData;
        }
    }

    // Gets data for all days within range
    public function getChartData(DataCruncherInterface $day, $start, $end) {

        /* Put data into array */
        /* [product_id][date => sales] */
        /*
        Ex.
        [
          1 => [
            "Jan 15" => 6946.27
            "Jan 16" => 6942.44
            ...
          ]
          2 => [
            "Jan 15" => 2039.13
            "Jan 16" => 1713.82
            ...
          ]
          ...
        ]
        */

        $currentDay = new Carbon($start);
        $lastDay = new Carbon($end);
        $allDaysData = null;

        // Get data for one day at a time
        while ($currentDay->lessThan($lastDay)) {

            // Get data for the day
            $dayData = $day->crunchData($currentDay, $this->labels);

            // Add results to running tally
            $allDaysData = $this->addDayToResults($dayData, $allDaysData);

            // Increment day
            $currentDay->addDay();
        }

        return $allDaysData;
    }

    public function getChartLabels() {
        return $this->labels;
    }

}