<?php

namespace App\Charts\Helpers;

class GraphFormatter
{
    // Count how many data points we're sending
    private $counter;

    public function __construct() {
        $counter = 0;
    }

    // Assign a different color to each series
    public function generateLineColor() {
        $this->counter++;
        switch ($this->counter) {
            case 1:
                return '255,99,132';
            case 2:
                return '54,162,235';
            case 3:
                return '255,206,86';
            case 4:
                return '75,192,192';
            case 5:
                return '153,102,255';
            case 6:
                return '255,159,64';
            case 7:
                return '138,100,100';
            case 8:
                return '0,255,78';
        }
        return rand(0,255) . ',' . rand(0,255) . ',' . rand(0,255);
    }

    // Convert input into ChartJS data
    public function generateGraph($products, $labels) {

        $data = [];

        // Create labels (dates as strings)
        $data['labels'] = [];
        foreach ($products as $product) {
            foreach ($product as $date => $amount) {
                $data['labels'][] = $date;
            }
            break;
        }

        // Create datasets
        $data['datasets'] = [];

        // Prepare data
        foreach($products as $productKey => $product) {
            $temp = [];

            // Fill up datasets
            $temp['data'] = [];
            foreach ($product as $date => $amount) {
                $temp['data'][] = $amount;
            }

            // Set graph options
            $temp['label'] = $labels[$productKey];
            $color = $this->generateLineColor();
            $temp['borderColor'] = 'rgba(' . $color . ',1)';
            $temp['backgroundColor'] = 'rgba(' . $color . ',0.1)';
            $data['datasets'][] = $temp;
        }

        return $data;
    }

}