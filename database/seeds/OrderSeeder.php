<?php

use Illuminate\Database\Seeder;

use App\Order;
use App\Customer;
use App\Lineitem;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = factory(Customer::class, 10000)->create();

        foreach ($customers as $customer) {

            $order = factory(Order::class)
                ->create([
                    'customer_id' => $customer->id,
                ]);

            // Add lineitems
            for ($i=0; $i<rand(1,6); $i++) {
                $order->lineitems()->save(
                    factory(Lineitem::class)->make()
                );
            }
        }
    }
}
