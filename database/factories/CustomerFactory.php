<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {

	$num = rand(1, 5); // US or Canada
	switch ($num) {
		case 1:
			$country = 'CA';
			break;
		default:
			$country = 'US';
	}

    return [
    	'first_name' => $faker->firstName,
    	'last_name' => $faker->lastName,
    	'address' => $faker->streetAddress,
    	'city' => $faker->city,
    	'state' => $faker->state,
    	'zip' => $faker->postcode,
    	'country' => $country,
    ];
});
