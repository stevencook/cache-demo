<?php

use Faker\Generator as Faker;

$factory->define(App\Lineitem::class, function (Faker $faker) {

	$productId = rand(1, 4); // 4 imaginary products
	switch ($productId) {
		case 1:
			$price = (int)(3000 + rand(0, 1000));
			break;
		case 2:
			$price = (int)(500 + rand(0, 1000));
			break;
		case 3:
			$price = (int)(1500 + rand(0, 1000));
			break;
		case 4:
			$price = (int)(2000 + rand(0, 2000));
			break;
		default:
			$price = 1000;
	}
	$quantity = rand(1, 3);

    return [
    	'product_id' => $productId,
    	'price_per_unit_in_cents' => $price,
    	'quantity' => $quantity,
    ];
});
