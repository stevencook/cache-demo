<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'created_at' => $faker->dateTimeBetween('-3 weeks', 'now', null),
    ];
});
