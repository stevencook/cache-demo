<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .content {
                text-align: center;
                max-width: 1200px;
                margin: 0 auto;
            }

            #endpoint {
                ;
            }
        </style>

        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                console.log('ready');

                var ctx = $('#myChart');
                var endpoint = $('#endpoint').data('endpoint');

                $.ajax({
                    url: endpoint,
                    success: function(result) {

                        var myChart = new Chart(ctx, {
                            type: 'line',
                            data: result,
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true,
                                            callback: function(value, index, values) {
                                                if(parseInt(value) >= 1000){
                                                    return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                } else {
                                                    return '$' + value;
                                                }
                                            }
                                        }
                                    }]
                                },
                                tooltips: {
                                    callbacks: {
                                        label: function(tooltipItem, data) {
                                            return '$' + tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        },
                                    },
                                }
                            }
                        });

                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="content">
            <div id="endpoint" data-endpoint="{{ $endpoint }}"></div>
            <canvas id="myChart" width="1200" height="600"></canvas>
        </div>
    </body>
</html>
